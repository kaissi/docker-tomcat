#!/bin/sh -x
TOMCAT_URL=http://archive.apache.org/dist/tomcat/tomcat-8/v8.5.9/bin/apache-tomcat-8.5.9.tar.gz
TOMCAT_DIR=/srv/tomcat
RUNNER_URL=http://search.maven.org/remotecontent?filepath=com/github/jsimone/webapp-runner/8.5.9.0/webapp-runner-8.5.9.0.jar
RUNNER_DIR=/srv/webapp-runner

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common
add-apt-repository ppa:webupd8team/java
apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
DEBIAN_FRONTEND=noninteractive apt-get -y install oracle-java8-installer
DEBIAN_FRONTEND=noninteractive apt-get -y install oracle-java8-set-default
DEBIAN_FRONTEND=noninteractive apt-get -y install curl
apt-get clean

mkdir $TOMCAT_DIR
curl $TOMCAT_URL | tar -xz -C $TOMCAT_DIR --strip-components=1 --exclude webapps/*
mkdir $RUNNER_DIR
curl $RUNNER_URL -L -o $RUNNER_DIR/webapp-runner.jar
